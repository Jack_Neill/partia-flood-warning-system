"""Unit test for the station module"""

import pytest
from floodsystem import station
from floodsystem.station import MonitoringStation
from floodsystem import stationdata
import random


def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town

def test_inconsistent_typical_range_stations():

    #build station data
    stations = stationdata.build_station_list()

    #create list of inconsistent typical range stations
    output_list = station.inconsistent_typical_range_stations(stations)

    #check that list has elements in it
    assert len(output_list) != 0

    #check that stations in the list actually have inconsistent data
    for x in stations:
        if x in output_list:
            assert x.typical_range == None or x.typical_range[0] > x.typical_range[1]
    
    
def test_relative_water_level():
    "returns the latest water level as a fraction of the typical range"
    
    #build station data
    stations = stationdata.build_station_list()
        
    for x in range(100):
        test_level = MonitoringStation.relative_water_level(stations[x])
        if test_level != None:
            assert type(test_level) is float == True
            assert test_level >= 0
            
        
    
    
    
    
    
