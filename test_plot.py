"""Test for the geo module"""

import pytest
from floodsystem import plot

from floodsystem.stationdata import build_station_list

#from floodsystem.plot import plot_water_levels
#from floodsystem.plot import plot_water_levels_general

from floodsystem.datafetcher import fetch_measure_levels

import datetime



def test_plot_water_levels():
    stations = build_station_list()

    dt = 10

    dates, levels = fetch_measure_levels(stations[0].measure_id,
                                                 dt=datetime.timedelta(days=dt))

    assert dates != None
    assert levels != None
    
    try:
        plot.plot_water_levels(stations[0], dates, levels)
        #plot.plt.close('all')
    except:
        assert False

def test_plot_water_levels_general():
    stations = build_station_list()

    station_data = []
    
    for station in stations[:6]:
        dt = 10

        dates, levels = fetch_measure_levels(station.measure_id,
                                                     dt=datetime.timedelta(days=dt))

        assert dates != None
        assert levels != None

        station_data.append((station,dates,levels))
    
    try:
        plot.plot_water_levels_general(station_data)
    except:
        assert False
   
def test_plot_water_level_with_fit():

    stations = build_station_list()

    dt = 10

    dates, levels = fetch_measure_levels(stations[0].measure_id,
                                                 dt=datetime.timedelta(days=dt))

    assert dates != None
    assert levels != None
    
    try:
        plot.plot_water_level_with_fit(stations[0], dates, levels,4)
        #plot.plt.close('all')
    except:
        assert False
