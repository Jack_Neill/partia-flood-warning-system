"""Test for the geo module"""

import pytest
from floodsystem import geo
from floodsystem.stationdata import build_station_list


def test_stations_by_distance():
    """Tests that when stations_by_distance is given 
    a list of stations and a coordinate p, 
    returns a list of (station, distance) tuples, 
    where distance (float) is the distance of the 
    station (MonitoringStation) from the coordinate p. 
    The returned list should be sorted by distance."""
    
    stations = build_station_list()
   
    test_stations = stations[0:10]
    test_coord = stations[2].coord
    
    test_output = geo.stations_by_distance(test_stations,test_coord)
    
    #Test 1nd station in list has a distance smaller than all proceding
    for i in test_output:
        assert test_output[0][1] <= i[1]
    #Test distance of station from itself is 0 and ordered at the start of list output
    assert test_output[0][1] == 0


def create_test_sample(N=10):

    stations = build_station_list()

    return stations[:N]

def test_stations_within_radius():
    """Tests that stations_within_radius returns a list of all 
    stations within radius 'r' of a geographic coordinate 'centre'."""
     
    stations = build_station_list()
   
    test_stations = stations[0:10]
    test_coord = stations[2].coord
    
    test_output1 = geo.stations_by_distance(test_stations,test_coord)
    test_output2 = geo.stations_within_radius(test_stations,test_coord,100)
    
    #Test all of output are closest to the coord
    for i in range(len(test_output2)):
        assert test_output1[i][0] == test_output2[i]

    #Test farthest station is withn radius
    assert test_output1[len(test_output2)-1][1] <= 100


def test_rivers_with_station():

    #build list of stations and create a small sample size of stations to test
    test_stations = create_test_sample()


    #initialise a empty list
    test_river = []

    #add the river names into a list so that we can compare it with the output
    #from the rivers_with_station function
    for x in test_stations:
        test_river.append(x.river)

    #create a set of rivers using the rivers with station function
    #test that the length of the set created is not zero
    rivers = geo.rivers_with_station(test_stations)
    assert len(rivers) != 0

    #test against the sample list to make sure that the river names are correctly
    #inputted into set of rivers
    for x in rivers:
        assert x in test_river

def test_stations_by_river():

    #create test sample
    test_stations = create_test_sample()

    #create dictionary using stations_by_river function
    river_dictionary = geo.stations_by_river(test_stations)

    #test that each station's key exists in the dictionary
    #test that each station's name is correctly mapped to the river name
    for x in test_stations:
        assert x.river in river_dictionary
        assert x.name in river_dictionary[x.river]


def test_rivers_by_station_number():

    #create test sample
    test_stations = create_test_sample(100)

    #create output list using rivers_by_station_number function
    output_list = geo.rivers_by_station_number(test_stations,5)

    #test that rivers have been sorted in descending order
    for n,x in enumerate(output_list):
        print (x)
        if n > 0:
            assert x[1] <= output_list[n-1][1]

    #test that the first river in the output list has the correct
    #number of stations

    count = 0
    
    for x in test_stations:
        if x.river == output_list[0][0]:
            count = count + 1

    assert count == output_list[0][1]
    
        


    
