from floodsystem import geo
from floodsystem.stationdata import build_station_list


p = (52.2053, 0.1218) 

def run():
    """Requirements for Task 1A"""

    # Build list of stations
    stations = build_station_list()

    print(geo.stations_by_distance(stations, p)[:10])
    print(geo.stations_by_distance(stations, p)[-10:])

   
if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")

    # Run Task1B
    run()
