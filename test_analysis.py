"""Test for the geo module"""

import pytest
from floodsystem import analysis

from floodsystem.stationdata import build_station_list

from floodsystem.datafetcher import fetch_measure_levels

import datetime



def test_polyfit():

    stations = build_station_list()

    dt = 10

    dates, levels = fetch_measure_levels(stations[0].measure_id,
                                                 dt=datetime.timedelta(days=dt))

    poly, d0 = analysis.polyfit(dates,levels, 4)

    # check that coefficients of poly is not zero
    assert poly(2) != 2

    #check that offset exists
    assert d0 != 0
    
