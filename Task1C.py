from floodsystem import geo
from floodsystem.stationdata import build_station_list


centre = (52.2053, 0.1218)
r = 10

def run():
    """Requirements for Task 1A"""

    # Build list of stations
    stations = build_station_list()

    print(geo.stations_within_radius(stations, centre, r))

   
if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")

    # Run Task1C
    run()
