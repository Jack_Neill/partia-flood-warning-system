import datetime
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot_plotly import plot_water_levels
from floodsystem.plot_plotly import plot_water_levels_general
from floodsystem.flood import stations_highest_rel_level

def run():

    # Build list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)

    station_data = []

    '''
    #Original implementation of plot water levels.
    at_risk_stations = stations_highest_rel_level(stations, 5)

    for station in at_risk_stations:
        dt = 10

        for name in stations:
            if name.name == station[0]:
                dates, levels = fetch_measure_levels(name.measure_id,
                                                 dt=datetime.timedelta(days=dt))

                plot_water_levels(name,dates,levels)
                
                break
            
        
        
        
    '''
    #general implementation of plot water levels.
    at_risk_stations = stations_highest_rel_level(stations, 5)

    for station in at_risk_stations:
        dt = 10

        for name in stations:
            if name.name == station[0] and name.name != 'Bampton Grange':
                dates, levels = fetch_measure_levels(name.measure_id,
                                                 dt=datetime.timedelta(days=dt))

                if(len(dates)!=0):
                
                    station_data.append((name,dates,levels))

                break
    
    plot_water_levels_general(station_data)
    


if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")

    run()
