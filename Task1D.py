from floodsystem import geo
from floodsystem.stationdata import build_station_list

def run():
    """Requirements for Task 1D"""

    # Build list of stations
    stations = build_station_list()

    #assemble list of rivers using rivers_with_station function
    rivers = geo.rivers_with_station(stations)

    #sort the list of rivers
    output_list = sorted(rivers)

    #print first 10 rivers
    print("Test of rivers_with_station function")
    
    print('Number of stations: ')
    print (len(output_list))
    
    print(output_list[:10])

    print()

    '''Second part of Requirements for Task 1D'''

    #create dictionary of rivers --> stations along rivers using function
    river_dictionary = geo.stations_by_river(stations)
    
    
    #sorts each list of river dictionaries
    list1 = []
    for x in river_dictionary['River Cam']:
        list1.append(x)
        
    list1.sort()
    
    list2 = []
    for x in river_dictionary['River Aire']:
        list2.append(x)
        
    list2.sort()
    
    list3 = []
    for x in river_dictionary['Thames']:
        list3.append(x)
        
    list3.sort()


    print("Test of stations_by_river function")
    print(list2)
    print()
    print(list1)
    print()
    print(list3)
    


if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")

    ''' add something to check if cache is present'''

    # Run Task1D
    run()
