from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold_edited, stations_highest_rel_level_edited
from floodsystem.floodtesting import extract_data
from floodsystem.plot_plotly import plot_in_map


#from floodsystem.flood import station_highest_rel_level


def run():
    """List of Highest Risk Stations. Ordered.
    Differentiate polynomial of past water level
    Extrapolate data over 2 days using gradient
    Create new tuples with (Name, Current relative level, differetiated water level, time spent above typical high range)
    Plots online in plotly"""
        
    # Build list of stations
    stations = build_station_list()
    
    # Update latest level data for all stations
    update_water_levels(stations)


    #Create list of top 30 stations with water level over 0.8
    #highest_levels = stations_level_over_threshold_edited(stations,0.8,30)
    highest_levels = stations_highest_rel_level_edited(stations,30)          
    
    
    data_list = extract_data(highest_levels)
    
    #data processing here


    #sort the list by relative water level to make it easier to visualise (optional)
    rel_level_sorted = sorted(data_list, key=lambda tup: tup[1], reverse=True)    

    #remove all stations where the water level is dropping
    filter_by_differentiation = []
    for i in rel_level_sorted:
        if(i[3] > 0):
            filter_by_differentiation.append(i)

    #sort the filtered list by the time each station spent above its typical high range
    final_sorted = sorted(filter_by_differentiation, key=lambda tup: tup[4], reverse=True)

    #print the data collected for checking purposes
    print("Name of station                    ", "Rel. Water level","\t\t","Differentiated Val","\t\t","Integrated Val")
    for i in final_sorted:
        temp = i[0].name
        while len(temp) <35:
            temp = temp + " "
        print(temp,round(i[1],2),'\t\t\t',round(i[3],2),'\t\t\t\t',round(i[4],2))

    #plot the final map on plotly
    plot_in_map(final_sorted, "Map of sites most likely to flood")
    
    
if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")

    run()
