from floodsystem.stationdata import build_station_list
from floodsystem import station


def run():
    """Requirements for Task 1F"""

    # Build list of stations
    stations = build_station_list()

    # Display data from 3 stations:
    '''for station in stations:
        if not station.typical_range_consistent():
            print (station.name)'''

    print("Prints stations with inconsistent data")
    print (station.inconsistent_typical_range_stations(stations))


if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")

    # Run Task1F
    run()
