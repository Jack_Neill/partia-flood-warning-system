import datetime
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_level_with_fit,plot_water_level_with_extrapolated_fit
from floodsystem.flood import stations_highest_rel_level
import matplotlib


def run():

    # Build list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)

    #For 5 stations at which current relative water level is highest

    count = 0
    
    for station in stations_highest_rel_level(stations,30):

        count = count + 1

        if count > 1:
            pass
        dt = 5

        for name in stations:
            if name.name == station[0]:
        
                dates, levels = fetch_measure_levels(name.measure_id,
                                                         dt=datetime.timedelta(days=dt))

                if len(dates) != 0:

                

                    plot_water_level_with_fit(name,dates,levels,4)

                
                #plot_water_level_with_extrapolated_fit(name,dates,levels,4,2)

                break
    
        
if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")

    run()
