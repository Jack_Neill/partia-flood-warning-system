"""Test for the geo module"""

import pytest
from floodsystem import flood
from floodsystem.stationdata import build_station_list, update_water_levels

def test_stations_level_over_threshold():
    """Tests all levels are sorted and larger than tolerance"""
    tol=0.8
    
    stations = build_station_list()
    
    update_water_levels(stations)
    
    test_output = flood.stations_level_over_threshold(stations,tol)
    
    y=test_output[0][1]    
    for x in range(len(test_output)-1):
        assert test_output[x][1]<=y
        y=test_output[x][1]
    assert y>tol
    
def test_stations_highest_rel_level():
    """Tests all levels are sorted and list has a length of N"""
    
    N=10
    
    stations = build_station_list()
    
    update_water_levels(stations)
    
    test_output = flood.stations_highest_rel_level(stations, N)
    
    y=test_output[0][1]    
    for x in range(len(test_output)-1):
        assert test_output[x][1]<=y
        y=test_output[x][1]
    assert len(test_output)==N
    