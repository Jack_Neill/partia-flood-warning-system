# CUED Part IA Flood Warning System 
## By Choon Kiat Lee and Jack Neill
---
This is the Part IA Lent Term computing activity at the Department of
Engineering, University of Cambridge.

## View the daily floodsystem map here! 
### https://plot.ly/~thevillageidiot/6  
(Best viewed in Mozilla Firefox. *The map might not render in google chrome)

The interactive map is updated daily by a scheduled python script (Task2G.py) that can also be run anytime to update. 

Displayed on the map are rivers most likely to flood, colour coded by severity.

Initially a list of stations (ordered by highest relative water level) is created.   
By fitting a polynomial to the station's data, and checking the gradient at present, we can see if the water level is rising or falling. Stations with dropping water level are assumed to not be at risk and are removed from the list.   
We then integrate under the curve to check what proportion of the last 2 days the rivers have stayed above their maximum range. Rivers spending more time above the threshold are more likely to flood, and therefore are ranked high for severity.   
Finally these stations are pushed to the online interactive map.  
*Link above.*


**Technical notes on the plotly map**   
The hovertext links are redirected to a javascript page(search.html) that parses the query and redirects the query to the the riverlevels.uk webpage. This eliminates the need for complicated code and makes the linking much easier through the plotly platform.