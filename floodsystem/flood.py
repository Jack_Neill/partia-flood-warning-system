"from station import MonitoringStation"

def stations_level_over_threshold(stations, tol):
    """returns a list of tuples with: (1) a station at which the 
    latest relative water level is over tol and (2) the relative 
    water level at the station. Sorted by relative level descending."""
    high_risk = []
    for station in stations:
        if station.relative_water_level() != None and station.relative_water_level() > tol:
            level = (station.name, station.relative_water_level())
            high_risk.append(level)

    high_risk_ordered = sorted(high_risk, key=lambda tup: tup[1], reverse=True)
    return high_risk_ordered
    
def stations_highest_rel_level(stations, N):
    """returns a list of the N stations at which the water level, 
    relative to the typical range, is highest. Sorted descending"""
    high_risk = []
    for station in stations:
        if station.relative_water_level() != None:
            level = (station.name, station.relative_water_level())
            high_risk.append(level)

    high_risk_ordered = sorted(high_risk, key=lambda tup: tup[1], reverse=True)

    top_ten = []
    
    for i in range(N):
        top_ten.append(high_risk_ordered[i])
        
    return top_ten

def stations_level_over_threshold_edited(stations, tol,N):
    """returns a list of tuples with: (1) a station at which the 
    latest relative water level is over tol and (2) the relative 
    water level at the station. Sorted by relative level descending."""
    high_risk = []
    for station in stations:
        if station.relative_water_level() != None and station.relative_water_level() > tol:
            level = (station, station.relative_water_level())
            high_risk.append(level)


    high_risk_ordered = sorted(high_risk, key=lambda tup: tup[1], reverse=True)

    return high_risk_ordered[:30]

def stations_highest_rel_level_edited(stations, N):
    """returns a list of the N stations at which the water level, 
    relative to the typical range, is highest. Sorted descending"""
    high_risk = []
    for station in stations:
        if station.relative_water_level() != None:
            level = (station, station.relative_water_level())
            high_risk.append(level)

    high_risk_ordered = sorted(high_risk, key=lambda tup: tup[1], reverse=True)

    top_ten = []
    
    for i in range(N):
        top_ten.append(high_risk_ordered[i])
        
    top_ten.pop(5)
        
    return top_ten
