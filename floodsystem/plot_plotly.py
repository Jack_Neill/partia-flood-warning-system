from datetime import datetime, timedelta
import numpy as np
from floodsystem.analysis import polyfit

import plotly
import plotly.plotly as plt
import plotly.graph_objs as go

from plotly.graph_objs import *


def plot_water_levels(station, dates, levels):

    '''plots the water levels per station'''


    plotly.tools.set_credentials_file(username='thevillageidiot', api_key='REvax6dRCN82rqyXn1wU')

    trace = go.Scatter(
        x = dates,
        y = levels
        )

    data = [trace]

    # Edit the layout
    layout = dict(title = 'Water Level at '+ station.name,
                  xaxis = dict(title = 'Date'),
                  yaxis = dict(title = 'Water Level (m)'),
                  )

    fig = dict(data=data, layout=layout)
    plt.plot(fig)


def plot_water_levels_general(station_data):

    '''plots the water levels for all the stations generally'''
    '''the function requires a list of station data and outputs a plotly graph'''

    plotly.tools.set_credentials_file(username='thevillageidiot', api_key='REvax6dRCN82rqyXn1wU')

    graph_data = []
    
    for data in station_data:

        # Plot

        graph_data.append(go.Scatter(x=data[1],y=data[2],name = data[0].name))


    # Edit the layout
    layout = dict(title = 'Graph of water level of various stations by date',
                  xaxis = dict(title = 'Date'),
                  yaxis = dict(title = 'Water Level (m)'),
                  )

    fig = dict(data=graph_data, layout=layout)
    plt.plot(fig)


def plot_in_map(sorted_list,file_name):

    '''
    Requires a list of stations and a file name to save the resulting plot under
    This function plots the list of stations in a scatter plot on a map, then saves it under the file name in plotly.
    '''
    
    
    
    plotly.tools.set_credentials_file(username='thevillageidiot', api_key='REvax6dRCN82rqyXn1wU')
    mapbox_access_token = 'pk.eyJ1IjoidGhldmlsbGFnZWlkaW90IiwiYSI6ImNpem9zdG10MjAwMDMycW15emV0aGJuZm8ifQ.TbT_WhYC3AVFqFl844TByQ'

    site_lat = []
    site_lon = []
    locations_name = []
    colours = []
    
    for i in sorted_list:

        site_lat.append(i[0].coord[0])
        site_lon.append(i[0].coord[1])
        locations_name.append("<a href = 'http://floodsystem.hol.es/search.html?"+i[0].name+"'>"+i[0].name + "   " + str(round(i[0].coord[0],2)) + "   " + str(round(i[0].coord[1],2))+"</a>")
            
        
        if(i[4]> 0):
            colours.append('rgb(255, 0, 0)')

        if(i[4]<0 and i[3]>1):
            colours.append('rgb(247, 200, 108)')
            
        else:
            colours.append('rgb(79, 161, 69)')
        

    data = Data([
    Scattermapbox(
        lat=site_lat,
        lon=site_lon,
        mode='markers',
        marker=Marker(
            size=8,
            color=colours,
            opacity=0.7
        ),
        text=locations_name,
        hoverinfo = 'text',
        showlegend=False
    ),
    Scattermapbox(  
        lat=site_lat,
        lon=site_lon,
        mode='markers',
        marker=Marker(
            size=17,
            color=colours,
            opacity=0.5
        ),
        hoverinfo='skip',
        showlegend=False
        ),
    ])

    layout = Layout(
    title='Colour coded map of sites most likely to flood <br>Click on the individual hovertext to search www.riverlevels.uk for the individual station!',
    autosize=True,
    hovermode='closest',
    mapbox=dict(
        accesstoken=mapbox_access_token,
        bearing=0,
        center=dict(
            lat=50,
            lon=0
        ),
        pitch=0,
        zoom=3,
        style='light'
        )
    )

    fig = dict(data=data, layout=layout)
    plt.plot(fig, filename=file_name)






    
