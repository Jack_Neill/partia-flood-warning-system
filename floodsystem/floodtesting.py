import matplotlib
import numpy as np
from floodsystem.analysis import polyfit
from floodsystem.datafetcher import fetch_measure_levels
import datetime
from itertools import chain


def extract_data(highest_levels):
    '''
    Function that extracts the required data from a list of stations,
    then it returns a list of tuples that contains the:
    station object, relative water level, polynomial obtained through polyfit, differentiated polynomial, time spent above typical high
    '''

    data_list = []

    #data extraction
    for station in highest_levels:

        extrapolated_dt = 2
        
        dates, levels = fetch_measure_levels(station[0].measure_id,
                                                         dt=datetime.timedelta(days=extrapolated_dt))       

        if len(dates) != 0:
            poly,d0 = polyfit(dates,levels,4)

            # Current time (UTC)
            now = datetime.datetime.utcnow()

            # Start time for data
            start = now - datetime.timedelta(days=extrapolated_dt)

            #Convert times to floats
            extrapolated_time = matplotlib.dates.date2num(start)

            now_float = matplotlib.dates.date2num(now)

            #Differentiate the polynomial obtained through polyfit
            differentiated_poly = np.polyder(poly)

            #Integrate the polynomial obtained through polyfit
            integrated_poly = np.polyint(poly)

            #perform a definite integral over the 2 days and obtain time above typical high range
            definite_integral = integrated_poly(now_float-d0)-integrated_poly(extrapolated_time-d0)

            time_above_typical_high = definite_integral - station[0].typical_range[1]*(now_float-extrapolated_time)

            #place data into a tuple and append to a storage list that is later returned. 
            data_tuple = (station[0],station[0].relative_water_level(),poly,differentiated_poly(now_float-d0),time_above_typical_high)

            data_list.append(data_tuple)

    return data_list


def filter_by_differentiation(input_list):
    '''
    Removes all stations with a negative gradient from the input list.
    Returns a list of stations'''
    
    output = []

    for i in input_list:
        if(i[3] > 0):
            output.append(i)

    return output
