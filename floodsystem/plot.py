
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import matplotlib
import numpy as np
from floodsystem.analysis import polyfit
import datetime


def plot_water_levels(station, dates, levels):

    # Plot
    plt.plot(dates, levels)

    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45);
    plt.title(station.name)

    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.show()


def plot_water_levels_general(station_data):

    for data in station_data:

        # Plot
        plt.plot(data[1], data[2],label = data[0].name)
    

    # Plot
    #plt.plot(dates, levels)

    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45);
    plt.title('Graph of water level of various stations by date')

    # Now add the legend with some customizations.
    legend = plt.legend(shadow=False)

    # The frame is matplotlib.patches.Rectangle instance surrounding the legend.
    frame = legend.get_frame()
    frame.set_facecolor('0.90')

    # Set the fontsize
    for label in legend.get_texts():
        label.set_fontsize('large')

    for label in legend.get_lines():
        label.set_linewidth(1.5)  # the legend line width
    
    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.show()

def plot_water_level_with_fit(station, dates, levels, p):

    #Use polyfit function to find least squares fit 
    poly,d0 = polyfit(dates,levels,4)

    #Plot the original data first
    plt.plot(dates,levels)


    #Build a list of dates converted to floats using Matplotlib
    dateFloats = matplotlib.dates.date2num(dates)

    # Plot polynomial fit at 30 points along interval 
    x1 = np.linspace(dateFloats[0], dateFloats[-1], 30)

    #Convert dateFloats back into dates to make plots more readable
    plotDates = []
    
    for i in x1:
        plotDates.append(matplotlib.dates.num2date(i))


    #plot best fit polynomial on the same graph as the original
    plt.plot(plotDates, poly(x1 - d0))

    #Add typical high/low range on the plot
    plt.axhspan(station.typical_range[0],station.typical_range[1],facecolor='#2ca02c', alpha=0.5)

    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45);
    plt.title('Graph of water level at ' + station.name + ' by date')

    #Show the graph. 
    plt.tight_layout()
    plt.show()


def plot_water_level_with_extrapolated_fit(station, dates, levels, p,extrapolated_dt):

    #Use polyfit function to find least squares fit 
    poly,d0 = polyfit(dates,levels,4)

    #Plot the original data first
    plt.plot(dates,levels)


    #Build a list of dates converted to floats using Matplotlib
    dateFloats = matplotlib.dates.date2num(dates)


    

    # Current time (UTC)
    now = datetime.datetime.utcnow()

     # Start time for data
    end = now + datetime.timedelta(days=extrapolated_dt)

    extrapolated_time = matplotlib.dates.date2num(end)


    # Plot polynomial fit at 30 points along interval 
    x1 = np.linspace(dateFloats[0], extrapolated_time, 30)
    

    #Convert dateFloats back into dates to make plots more readable
    plotDates = []
    
    for i in x1:
        plotDates.append(matplotlib.dates.num2date(i))


    #plot best fit polynomial on the same graph as the original
    plt.plot(plotDates, poly(x1 - d0))

    #Add typical high/low range on the plot
    plt.axhspan(station.typical_range[0],station.typical_range[1],facecolor='#2ca02c', alpha=0.5)

    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45);
    plt.title('Graph of water level at ' + station.name + ' by date')

    #Show the graph. 
    plt.tight_layout()
    plt.show()







    
