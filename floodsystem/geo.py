"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key
from haversine import haversine

def rivers_with_station(stations):

    '''build and return a set of all rivers that have a monitoring station
    attached to them

    Takes a list of stations as input and returns a set of rivers as output
    '''


    #use list comprehension to generate a set of rivers within each station
    rivers = { x.river for x in stations}

    return rivers


def stations_by_river(stations):

    ''' build a dictionary that maps river names to a list of stations along
    the river'''
    

    output_dictionary = {}

    for station in stations:

        if station.river in output_dictionary:
            output_dictionary[station.river].append(station.name)

        else:    
            output_dictionary[station.river] = [station.name]

    return output_dictionary


def rivers_by_station_number(stations,N):

    #create dictionary of rivers using stations_by_river function
    river_dictionary = stations_by_river(stations)

    #initialise empty output_list
    river_list = []   

    #create output_list
    for river in river_dictionary:
        
        river_list.append((len(river_dictionary[river]),river))

    #sort river list in descending order
    river_list.sort(reverse = True)

    #initialise variables required for for loop
    output_list = []

    count = 0

    #loop through the for loop and add the appropriate number of rivers.
    #remember to flip the tuple!
    for i in river_list:
        if count >= 9 :
            if i[0] == output_list[8][1]:
                output_list.append((i[1],i[0]))
                count = count + 1
            else:
                break
        else:
            output_list.append((i[1],i[0]))
            count = count + 1
       
        
    return output_list
    


def stations_by_distance(stations, p):
    """Given a list of stations and a coordinate p, 
    returns a list of (station, distance) tuples, 
    where distance (float) is the distance of the 
    station (MonitoringStation) from the coordinate p. 
    The returned list should be sorted by distance."""
    station_list = []
   
    #Creates tuple of all stations that have both a name and coordiate, then appends to list
    for station in stations:
        
        try:
            
            distance = haversine(station.coord,p)
            station_tuple = (station.name,distance)
            station_list.append(station_tuple)
        except:
            pass
    #Sorts list
    station_list_ordered = sorted(station_list, key=lambda tup: tup[1])
    
    return station_list_ordered 
    

def stations_within_radius(stations, centre, r):
    """returns a list of all stations within 
    radius 'r' of a geographic coordinate 'centre'."""
    stations_within_radius = []
    station_list_ordered = stations_by_distance(stations,centre)
    
    #Creates list of station names within radius. Stops when distances get larger than r
    for station in station_list_ordered:
        if station[1] < r:
            stations_within_radius.append(station[0])
        
        else:
            break
        
    stations_within_radius.sort()
    
    return stations_within_radius



    
